# Translations

These Bible and Quran translations were provided by the authors with permission. Please see the individual translations authors for details on using in your own project.

### Available Translations

Here is a list of the available translations and their accessibility online as well as the formatted download link. You will find a `metadata.json` file for processing the files programmatically.

| Id      | Name                                                        | Original | Direction | Language | Type   | Filename                                                                       |
| ------- | ----------------------------------------------------------- | -------- | --------- | -------- | ------ | ------------------------------------------------------------------------------ |
| gnt     | [Βίβλος](https://alkotob.org/r/gnt/)                        | true     | ltr       | gr       | injil  | [gnt.xml](https://alkotob.gitlab.io/translations/data/injil/gnt.xml)           |
| injil   | [الإنجيل](https://alkotob.org/r/injil/)                    | false    | rtl       | ar       | injil  | [injil.xml](https://alkotob.gitlab.io/translations/data/injil/injil.xml)       |
| injilen | [The Honored Injil](https://alkotob.org/r/injilen/)         | false    | ltr       | en       | injil  | [injileng.xml](https://alkotob.gitlab.io/translations/data/injil/injileng.xml) |
| sabeel  | [السَّبِيْلُ](https://alkotob.org/r/sabeel/)                | false    | rtl       | ar       | injil  | [sabeel.xml](https://alkotob.gitlab.io/translations/data/injil/sabeel.xml)     |
| sbleng  | [The Path](https://alkotob.org/r/sbleng/)                   | false    | ltr       | en       | injil  | [sbleng.xml](https://alkotob.gitlab.io/translations/data/injil/sbleng.xml)     |
| tma     | [المعنى الصحيح لإنجيل المسيح](https://alkotob.org/r/tma/)   | false    | rtl       | ar       | injil  | [tma.xml](https://alkotob.gitlab.io/translations/data/injil/tma.xml)           |
| qeng59  | [Yusuf Ali](https://alkotob.org/r/qeng59/)                  | false    | ltr       | en       | quran  | [qeng59.xml](https://alkotob.gitlab.io/translations/data/quran/qeng59.xml)     |
| qeng63  | [Transliteration](https://alkotob.org/r/qeng63/)            | false    | ltr       | en       | quran  | [qeng63.xml](https://alkotob.gitlab.io/translations/data/quran/qeng63.xml)     |
| qind68  | [Bahasa Indonesia](https://alkotob.org/r/qind68/)           | false    | ltr       | in       | quran  | [qind68.xml](https://alkotob.gitlab.io/translations/data/quran/qind68.xml)     |
| qref    | [The English Reference Qur’an](https://alkotob.org/r/qref/) | false    | ltr       | en       | quran  | [qref.xml](https://alkotob.gitlab.io/translations/data/quran/qref.xml)         |
| quran   | [القرآن الكريم](https://alkotob.org/r/quran/)               | true     | rtl       | ar       | quran  | [quran.xml](https://alkotob.gitlab.io/translations/data/quran/quran.xml)       |
| mzm     | [المزامير](https://alkotob.org/r/mzm/)                      | false    | rtl       | ar       | zabur  | [mzm.xml](https://alkotob.gitlab.io/translations/data/zabur/mzm.xml)           |
| zabur   | [الزَّبُورُ](https://alkotob.org/r/zabur/)                  | false    | rtl       | ar       | zabur  | [zabur.xml](https://alkotob.gitlab.io/translations/data/zabur/zabur.xml)       |
| wlc     | [תורה](https://alkotob.org/r/wlc/)                          | true     | rtl       | he       | tawrat | [wlc.xml](https://alkotob.gitlab.io/translations/data/tawrat/wlc.xml)          |

### Setup

```
yarn install
yarn build
```