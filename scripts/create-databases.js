const Config        = require('../metadata.json')
const path          = require('path')
const xml2js        = require('xml2js')
const fs            = require('fs')
const { promisify } = require('util')
const mkdirp        = require('mkdirp')
const sqlite3       = require('sqlite3').verbose()
const shortid       = require('shortid')
const async         = require('async')
const _             = require('lodash')
const git           = require('git-rev-sync')

const readFile    = promisify(fs.readFile)
const parser      = new xml2js.Parser()
const parseString = promisify(parser.parseString)

const dbDir = path.resolve(__dirname, '../build/databases')
mkdirp.sync(dbDir)

function initializeDB(db) {
  db.serialize(() => {
    db.run(`
      CREATE TABLE collections (
        id VARCHAR(10) PRIMARY KEY,
        name VARCHAR(255),
        language VARCHAR(5),
        type VARCHAR(10),
        direction VARCHAR(3),
        bismillah VARCHAR(255),
        metadata TEXT
      )
    `)

    db.run(`
      CREATE TABLE books (
        id VARCHAR(10) PRIMARY KEY,
        collection_id VARCHAR(10),
        reference VARCHAR(10),
        name,
        FOREIGN KEY(collection_id) REFERENCES collections(id)
      )
    `)

    db.run(`
      CREATE TABLE chapters (
        id VARCHAR(10) PRIMARY KEY,
        collection_id VARCHAR(10),
        book_id VARCHAR(10),
        number INT,
        name,
        FOREIGN KEY(book_id) REFERENCES books(id),
        FOREIGN KEY(collection_id) REFERENCES collections(id)
      )
    `)

    db.run(`
      CREATE TABLE verses (
        id VARCHAR(10) PRIMARY KEY,
        chapter_id VARCHAR(10),
        number INT,
        end INT,
        text TEXT,
        FOREIGN KEY(chapter_id) REFERENCES chapters(id)
      )
    `)

    db.run(`
      CREATE TABLE metadata (
        id VARCHAR(10) PRIMARY KEY,
        chapter_id VARCHAR(10),
        verse_id VARCHAR(10),
        type VARCHAR(20),
        location INT,
        indent INT,
        text TEXT,
        FOREIGN KEY(chapter_id) REFERENCES chapters(id),
        FOREIGN KEY(verse_id) REFERENCES verses(id)
      )
    `)
  })
}

// Initialize the master database
const dbAll = new sqlite3.Database(path.resolve(dbDir, `alkotob.sqlite`))
initializeDB(dbAll)

// A shorthand for inserting
function insert(db, sql, values) {

  // Inserts it into the localized database
  db.serialize(() => db.run(sql, values))

  // Inserts it into the master database
  dbAll.serialize(() => dbAll.run(sql, values))
}

async function getDatabase(id) {
  console.log(`- creating database for ${id}`)

  const dbFile = path.resolve(dbDir, `${id}.sqlite`)
  const db = new sqlite3.Database(dbFile)

  initializeDB(db)

  console.log(`- finished creating database for ${id}`)

  return db
}

function processChapters(db, { chapters }, collectionId, bookId = null) {
  if (chapters && chapters.length) chapters.forEach(({ chapter }) => {
    if (chapter && chapter.length) chapter.forEach(c => {
      const chapterId = shortid.generate()
      insert(db, 'INSERT INTO chapters VALUES (?, ?, ?, ?, ?)', [
        chapterId,
        collectionId,
        bookId,
        c.$.id,
        c.$.name
      ])

      processVerses(db, c, chapterId)
    })
  })
}

function processVerses(db, { verses }, chapterId = null) {
  if (verses && verses.length) verses.forEach(({ verse }) => {
    if (verse && verse.length) verse.forEach(v => {
      const verseId = shortid.generate()

      insert(db, 'INSERT INTO verses VALUES (?, ?, ?, ?, ?)', [
        verseId,
        chapterId,
        v.$.id,
        _.get(v, '$.end') || null,
        v.content[0].toString().trim()
      ])

      if (v.metadata) processMeta(db, v, chapterId, verseId)
    })
  })
}

function processMeta(db, { metadata }, chapterId = null, verseId = null) {
  if (metadata && metadata.length) metadata.forEach(({ item }) => {
    if (item) {
      item.forEach(m => {
        const metadataId = shortid.generate()

        insert(db, 'INSERT INTO metadata VALUES (?, ?, ?, ?, ?, ?, ?)', [
          metadataId,
          chapterId,
          verseId,
          _.get(m, '$.type') || null,
          _.get(m, '$.index') || null,
          _.get(m, '$.indent') || null,
          _.get(m, '_') || null
        ])
      })
    }
  })
}

async.eachLimit(Config.data, 1, async collection => {
  console.log(`+ processing ${collection.id}`)

  const filename = path.resolve(__dirname, '../data', collection.type, collection.filename)
  const data     = await readFile(filename)
  const parsed   = await parseString(data)

  // Get the collection database
  const db = await getDatabase(collection.id)

  const dbConfig = _.find(Config.data, { id: collection.id })

  // Create the collection in the database
  insert(db, 'INSERT INTO collections VALUES (?, ?, ?, ?, ?, ?, ?)', [
    collection.id,
    collection.name,
    collection.language,
    collection.type,
    collection.direction,
    collection.bismillah || null,
    JSON.stringify(dbConfig.metadata)
  ])

  if (collection.type !== 'quran') {
    parsed.collection.books.forEach(({ book }) => {
      book.forEach(b => {
        const bookId = shortid.generate()
        insert(db, 'INSERT INTO books VALUES (?, ?, ?, ?)', [
          bookId,
          collection.id,
          b.$.id,
          b.$.name
        ])

        processChapters(db, b, collection.id, bookId)
      })
    })
  } else {
    processChapters(db, parsed.collection, collection.id)
  }
}, (err) => {
  if (err) console.log(err)

  // Make metadata available on CDN with available databases
  const metafile = path.resolve(__dirname, '../build/databases', 'metadata.json')
  Config.version = git.long()
  Config.data = Config.data.map(item => {
    item.database = `${item.id}.sqlite`
    return item
  })

  fs.writeFileSync(metafile, JSON.stringify(Config, null, 2))
})
