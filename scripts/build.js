const xml2js        = require('xml2js')
const fs            = require('fs')
const { promisify } = require('util')
const _             = require('lodash')
const Utils         = require('./utils')

const readdir     = promisify(fs.readdir)
const readFile    = promisify(fs.readFile)
const parser      = new xml2js.Parser()
const parseString = promisify(parser.parseString)

const types        = ['injil', 'quran', 'zabur', 'tawrat']
const originals    = ['quran', 'gnt', 'wlc']
const rtlLanguages = ['ar', 'he']

// Create tasks for reading all types
const tasks = types.map(async type => {
  const dir     = `./data/${type}`
  const metaDir = `./metadata/${type}`

  let files = await readdir(dir)

  // Only show the XML files in case there are other source files
  files = files.filter(name => name.indexOf('.xml') !== -1)

  // Create tasks for reading all content in the XML files
  const readAll = files.map(async file => {
    const filename  = `${dir}/${file}`
    const data      = await readFile(filename)
    const parsed    = await parseString(data)
    const id        = _.get(parsed, 'collection.$.id')
    const language  = _.get(parsed, 'collection.$.language')
    const original  = originals.indexOf(id) !== -1
    const direction = rtlLanguages.indexOf(language) !== -1 ? 'rtl' : 'ltr'

    // Attach the metadata
    let metadata = {}
    const metaFile = `${metaDir}/${file.replace('.xml', '.json')}`

    if (fs.existsSync(metaFile)) {
      let loadedMeta  = await readFile(metaFile)
      metadata = JSON.parse(loadedMeta)
    }

    return {
      id: _.get(parsed, 'collection.$.id'),
      name: _.get(parsed, 'collection.$.name'),
      original,
      direction,
      language,
      type,
      filename: file,
      source: `https://alkotob.gitlab.io/translations/data/${type}/${file}`,
      metadata,
    }
  })

  return Promise.all(readAll)
})

Promise.all(tasks).then(async output => {
  const translations = _.flatten(output)

  await Promise.all([
    Utils.generateReadme(translations),
    Utils.generateMetadata(translations),
  ])

  console.log('+ finished')
})
