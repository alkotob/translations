const fs            = require('fs')
const path          = require('path')
const table         = require('markdown-table')
const humanize      = require('string-humanize')

/**
 * Generates the README for the actual root file system
 * @param {Array} translations Translation array generated from XML listing
 */
async function generateReadme(translations) {
  const filtered = translations.map(({metadata, source, ...row}) => {
    return row
  })

  let data = [Object.keys(filtered[0]).map(k => humanize(k))]
  filtered.forEach(t => {
    // Updates the file to be a link
    t.filename = `[${t.filename}](https://alkotob.gitlab.io/translations/data/${t.type}/${t.filename})`

    // Updates the file to be a link
    t.name = `[${t.name}](https://alkotob.org/r/${t.id}/)`

    return data.push(Object.values(t))
  })

  const content = `
# Translations

These Bible and Quran translations were provided by the authors with permission. Please see the individual translations authors for details on using in your own project.

### Available Translations

Here is a list of the available translations and their accessibility online as well as the formatted download link. You will find a \`metadata.json\` file for processing the files programmatically.

${table(data)}

### Setup

\`\`\`
yarn install
yarn build
\`\`\`
`.trim()

  const file = path.resolve(__dirname, '../README.md')
  fs.writeFileSync(file, content)
}

async function generateMetadata(translations) {
  const file = path.resolve(__dirname, '../metadata.json')
  fs.writeFileSync(file, JSON.stringify({
    timestamp: new Date().toISOString(),
    website: 'https://alkotob.org',
    data: translations
  }, null, 2))
}

module.exports = { generateReadme, generateMetadata }
